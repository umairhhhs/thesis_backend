var restful = require('node-restful');
var uniqueValidator = require('mongoose-unique-validator');
var mongoose = restful.mongoose;
var validateEmail = function(email) {

    var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

    return re.test(email)

};

// Schema
var userSchema = new mongoose.Schema({
    firstname: { type: String,  required: [true, 'First name must be provided'] },    
    lastname: { type: String,  required: [true, 'Last name must be provided'] },
    username: { type: String, required: true, unique: true },
    password: { type: String,  required: [true, 'Password must be provided'] }, 
    email: { type: String, index: true, required: true, unique: true },
    photoid: {type: String}
});
// Return model
userSchema.plugin(uniqueValidator);
module.exports = restful.model('Users', userSchema);

