// Dependencies
var restful = require('node-restful');
var mongoose = restful.mongoose;

// Schema
var annotatedTweetSchema = new mongoose.Schema({
    userId: String,
    tweetId: String,
    optionSelected: String
});

// Return model
module.exports = restful.model('AnnotatedTweets', annotatedTweetSchema);