var restful = require('node-restful');
var mongoose = restful.mongoose;


// Schema
var profileSchema = new mongoose.Schema({
    userid: { type: String, index: true, required: true, unique: true },    
    username: { type: String },
    questions: { type: Number },
    answers: { type: Number },
    thumbs: { type: Number }, 
    rank: { type: Number},
    points: {type: Number},
    ratings: {type: Number},
    photoid: {type: String}
});
// Return model
module.exports = restful.model('Profiles', profileSchema);

