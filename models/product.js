// Dependencies
var restful = require('node-restful');
var mongoose = restful.mongoose;

// Schema
var imageSchema = new mongoose.Schema({
    img: { data: Buffer, contentType: String }
});

// Return model
module.exports = restful.model('Images', imageSchema);