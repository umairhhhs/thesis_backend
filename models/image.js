// Dependencies
var restful = require('node-restful');
var mongoose = restful.mongoose;
var uniqueValidator = require('mongoose-unique-validator');


// Schema
var imageSchema = new mongoose.Schema({
    photo: {type: String, required: true},
    userid: {type: String ,index: true, required: true, unique: true }
});

imageSchema.plugin(uniqueValidator);
// Return model
module.exports = restful.model('Images', imageSchema);