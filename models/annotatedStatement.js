// Dependencies
var restful = require('node-restful');
var mongoose = restful.mongoose;

// Schema
var annotatedStatementSchema = new mongoose.Schema({
    userId: String,
    statementId: String,
    optionSelected: String
});

// Return model
module.exports = restful.model('AnnotatedStatements', annotatedStatementSchema);