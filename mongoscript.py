from pymongo import MongoClient
import random

client = MongoClient("mongodb://localhost:27017")
db = client.rest_test
statements = db.statements

trial_limit = 10

levels = ["easy","medium","hard", "unannotated"]
for statement in statements.find():
	level = "easy"
	if trial_limit > 0 and random.randint(1,5)==1:
		level = "trial"
		trial_limit = trial_limit-1
	else:
		level_index = random.randint(0,len(levels)-1)
		level = levels[level_index]
	db.statements.update({'_id':statement["_id"]}, {'$set':{'level':level}})
print("Trial statements added = "+str(10-trial_limit))
