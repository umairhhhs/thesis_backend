// Dependencies
var restful = require('node-restful');
var mongoose = restful.mongoose;
var random = require('mongoose-simple-random');

// Schema
var statementSchema = new mongoose.Schema({
    text: String,
    type: String,
    seenby: [],
    options: [],
    level: String
});
statementSchema.plugin(random);

// Return model
module.exports = restful.model('Statements', statementSchema);