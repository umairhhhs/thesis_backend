// Dependencies
var restful = require('node-restful');
var mongoose = restful.mongoose;
var random = require('mongoose-simple-random');

// Schema
var tweetSchema = new mongoose.Schema({
    time: String,
    text: String,
    handle: String,
    seenby: [],
    options: [],
    level: String
});
tweetSchema.plugin(random);

// Return model
module.exports = restful.model('Tweets', tweetSchema);