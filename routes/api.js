// Dependencies
var express = require('express');
var router = express.Router();
const random = require('random')


// Models
var Image = require('../models/image');
var Tweet = require('../models/tweet');
var Statement = require('../models/statement');
var User = require('../models/user');
var Profile = require('../models/profile');
var AnnotatedTweet = require('../models/annotatedTweet');
var AnnotatedStatement = require('../models/annotatedStatement');

// Routes
Statement.methods(['get', 'put', 'delete']);

Statement.route('annotate.post', function(req, res, next) {

    var annotation = new AnnotatedStatement();
    annotation.userId = req.body.userid;
    annotation.statementId = req.body.statementid;
    annotation.optionSelected = req.body.optionselected;
    annotation.save(function (err, user) {
      if (err) {res.json({ "Error" : err })}
        else{

Statement.findOne({_id: req.body.statementid }, {}, function(err, statement) {
    if (err) {res.json({ "Error" : err })}

       if (statement) {
        // we have a result
        statement.seenby.push(req.body.userid);
        statement.save();
        console.error('Success: Added UserId '+ req.body.userid  +' to seen for statementId '+ statement._id  );
       

    } else {
        // we don't
          // res.status(400);
          console.error('Failed: To add UserId '+ req.body.userid  +' to seen for statementId '+ statement._id  );
          // res.json('Username or Password is wrong' );
          next()
        }
  })

          console.error('Annotation details = ' + annotation );
          res.json(annotation);
        };  
});
});

Statement.route('getstatements.get', function(req, res, next) {
  // console.error('This is statement object  = ' + statement );
  
var fullResult = [];
Statement.count().exec(function (err, count) {


var options = { skip: random.int(min = 0, max = count -1), limit: 10, count: 5 };
Statement.findRandom({ "seenby": { "$ne": req.query.id }, "level": { "$in": req.query.level } }, {}, options, function(err, results) {
  if (err) res.json(err);
  else res.json(results);
 
});
});
});

Statement.register(router, '/statements');

Tweet.methods(['get', 'put', 'delete']);

Tweet.route('annotate.post', function(req, res, next) {

			var annotation = new AnnotatedTweet();
	  annotation.userId = req.body.userid;
    annotation.tweetId = req.body.tweetid;
    annotation.optionSelected = req.body.optionselected;
    annotation.save(function (err, user) {
      if (err) {res.json({ "Error" : err })}
      	else{

Tweet.findOne({_id: req.body.tweetid}, {}, function(err, tweet) {
    if (err) {res.json({ "Error" : err })}

       if (tweet) {
        // we have a result
        tweet.seenby.push(req.body.userid);
        tweet.save();
        console.error(' Success: Added tweetid to seen ' );
       

    } else {
        // we don't
          // res.status(400);
          console.error(' Failed: To Add tweetid to seen ' );
          // res.json('Username or Password is wrong' );
          next()
        }
  })

      		console.error('Annotation details = ' + annotation );
      		res.json(annotation);
      	};	
});
});

Tweet.route('gettweets.get', function(req, res, next) {
	// console.error('This is tweet object	= ' + tweet );
	
var fullResult = [];
Tweet.count().exec(function (err, count) {

var options = { skip: random.int(min = 0, max = count -1), limit: 10 };
Tweet.findRandom({ "seenby": { "$ne": req.query.id }, "level": { "$in": req.query.level } }, {}, options, function(err, results) {
  if (err) res.json(err);
  else res.json(results);
  
});
});
});

Tweet.register(router, '/tweets');


AnnotatedTweet.methods(['get', 'put', 'delete']);
AnnotatedTweet.register(router, '/annotations');

Profile.methods(['get', 'post', 'put', 'delete']);
Profile.register(router, '/profiles');


Image.methods(['get', 'post', 'delete']);
Image.route('getphoto.get', function(req, res, next) {

Image.findOne({_id: req.query.id}, {}, function(err, image) {
        console.error('image found with id '+ req.query.id );
    if (err) {res.json({ "Error" : err })}

      if (image) {
        console.error('user image returned ' + image._id );
        res.send(image.photo);
    } else {
       
        console.error('user image not returned' );
        next();
        }
  })

});
Image.route('photo', function(req, res, next) {

        // we have a result
     var image = new Image();

	   image.userid = req.body.userid;
     image.photo = req.body.photo;

    image.save(function (err, image) {
      if (err) {
      	console.error( 'This is Error' + err )
      	res.json( err )
     
      }else{

      User.updateOne(
     {_id: req.body.userid}, 
     {photoid : image._id }
     , 
       function(err, upsertedId){ 
       if (err) {
      	console.error( 'This is Error on updateing user photoid to userobject' + err )
      }
      	else{
      	console.error( 'This is result of updateing user photoid to userobject whose Id is' + upsertedId ) ;
      		} 
       
   }
   );


      Profile.updateOne(
     {userid: req.body.userid}, 
     {photoid : image._id}
     , 
       function(err, upsertedId){  
       	if (err) {
      	console.error( 'This is Error on updateing profile photoid to userobject' + err );
      }
      	else{
      	console.error( 'This is result of updateing profile photoid to profile whose Id is ' + upsertedId);
      		} 
       
       }
       );

      		res.json(image);


      	};

});
});

Image.register(router, '/images');

User.methods(['get', 'put', 'post', 'delete']);
User.route('login', function(req, res, next) {

User.findOne({username: req.body.username, password: req.body.password}, {}, function(err, user) {
    if (err) {res.json({ "Error" : err })}

    	if (user) {
        // we have a result
          console.error('	user object returned' + user );
      		res.json(user);
    } else {
        // we don't
        	res.status(400);
         	console.error('Username or Password is wrong' );
      		res.json('Username or Password is wrong' );
    		}
	})
});


User.route('signup', function(req, res, next) {
	var user = new User();

	  user.firstname = req.body.firstname;
    user.lastname = req.body.lastname;
    user.username = req.body.username;
    user.password = req.body.password;
    user.email = req.body.email;

    user.save(function (err, user) {
      if (err) {
      	console.error( 'This is Error on user' + err )
      	res.json( err )}
      	else{
      	console.error( 'This is result on user' +user );
	var profile = new Profile();

	  profile.userid = user._id;
    profile.username = user.username;
    profile.questions = 0;
    profile.answers = 0;
    profile.thumbs = 0;
    profile.rank = 0;
    profile.points = 0;
    profile.ratings = 0;

    profile.save(function (err, profile) {
      if (err) {
      	console.error( 'This is Error on profile' + err )
      	res.json( err )}
      	else{
      	console.error( 'This is result on profile' +profile );
      	};

});
    res.json(user);

      	};

});
});
User.register(router, '/users');


// Return router
module.exports = router;